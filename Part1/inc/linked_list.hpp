#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>

template <class T>
struct Node {
        T data;
        Node<T> *next;
        ~Node() {
            // if(sizeof(data) == sizeof(void*))
            //     delete data;
        }
};


template <class T>
class LinkedList {
    private:
        Node<T> *head;
    
    public:
        LinkedList() {
            head = NULL;
        }

        void insert(T data) {
            if(head == NULL) {
                head = new Node<T>;
                head->data = data;
                head->next = NULL;
            }
            else {
                Node<T> *temp = head;
                while(temp->next != NULL) {
                    temp = temp->next;
                }
                temp->next = new Node<T>;
                temp->next->data = data;
                temp->next->next = NULL;
            }
        }

        Node<T>* begin() {
            return head;
        }

        ~LinkedList() {
            while(head != NULL) {
                Node<T> *tmp=head;
                head = head->next;
                delete tmp;
            }
            delete head;
        }
};

#endif