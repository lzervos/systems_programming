struct Transaction;

struct bitCoinTransaction {
    Transaction* tr;
    int value;
};

struct node {
    bitCoinTransaction* bt;
    node *left;
    node *right;
};

class Tree {
    private:
        node *root;
        
    public:

        Tree(int);
        void insert(bitCoinTransaction*);
         ~Tree();
};