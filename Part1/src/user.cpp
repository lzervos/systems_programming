#include <iostream>
#include <cstdlib>
#include <cstring>
#include "../inc/user.hpp"
#include "../inc/bitcoin.hpp"
#include "../inc/myvector.hpp"


/*
 * Simple user functions
 * Not worth of explanation
 */

User::User(char *id) {
    userId = (char*) malloc((strlen(id) + 1) * sizeof(char));
    strcpy(userId,id);
    bitCoinList = new MyVector<Bitcoin*>;
    balance = 0;
}

MyVector<Bitcoin*>* User::getBitCoinList() {
    return bitCoinList;
}

void User::addBitCoin(Bitcoin *bitCoin) {
    bitCoinList->add(bitCoin);
    balance += bitCoin->currentValue;
}

void User::setBalance(int amount) {
    balance += amount;
}

int User::getBalance() {
    return balance;
}

char* User::getUserId() {
    return userId;
}

User::~User() {
    free(userId);
    bitCoinList->clear();
    delete bitCoinList;
}
