#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include "../inc/transaction.hpp"
#include "../inc/hash.hpp"
#include "../inc/functions.hpp"
#include "../inc/bitcoin.hpp"
#include "../inc/user.hpp"
#include "../inc/linked_list.hpp"
#include "../inc/myvector.hpp"


int main(int argc, char **argv)
{

    int bitCoinValue;
    char option;
    size_t senderHashtableNumOfEntries, receiverHashtableNumOfEntries, bucketSize;
    char *bitCoinBalancesFl, *transactionsFl;

    /*
     * Here I am checking/parsing program arguments
     */
    while ((option = getopt(argc, argv, "a:t:v:s:r:b:")) != -1)
        switch (option)
        {
        //flags for arguments
        case 'a':
            bitCoinBalancesFl = optarg;
            break;
        case 't':
            transactionsFl = optarg;
            break;
        case 'v':
            bitCoinValue = atoi(optarg);
            break;
        case 's':
            senderHashtableNumOfEntries = atol(optarg);
            break;
        case 'r':
            receiverHashtableNumOfEntries = atol(optarg);
            break;
        case 'b':
            bucketSize = atol(optarg);
            break;
        default:
            break;
        }

    //Condition for wrong arguments
    if (argv[optind] == NULL && optind < argc) {
        std::cerr << "Wrong or missing arguments" << std::endl;
        return 1;
    }

    FILE *bitCoinBalancesFile;
    ssize_t read; 
    size_t len = 0;
    char *line = NULL;

    if((bitCoinBalancesFile = fopen(bitCoinBalancesFl,"r")) == NULL) {
        perror("There was problem with the bitCoinBalance file");
        return 1;
    }

    MyVector<User*> vct; // Vector with Users where we store his 'status'
    

    /*
     * Reading User properties
     * userId and list of bitcoins he has initially
     */
    while ((read = getline(&line, &len, bitCoinBalancesFile)) !=-1) {
        line = strtok(line,"\n");

        char *token = strtok(line,DELIM);
        /**
         * Checking if userId exists already
         */
        if(!isValidUserId(vct,token)) {
            std::cerr << "There is already a user with userId " << token << std::endl;
            fclose(bitCoinBalancesFile);
            vct.clear();
            free(line);
            return 0;
        }

        User *user = new User(token);

        /*
         * Parsing his bitcoins
         */
        while(token != NULL) {
            token = strtok(NULL,DELIM);
            if(token!=NULL) {
                Bitcoin *bitCoin = new Bitcoin(atoi(token),bitCoinValue);
                /* Checking if bitcoinId exists already either in this user,or in other users
                 */
                if(!isValidBitCoin(vct,user,bitCoin->bitCoinId)) {
                    std::cerr << "There is already a bitCoin with bitCoinId " << bitCoin->bitCoinId << std::endl;
                    fclose(bitCoinBalancesFile);
                    vct.clear();
                    delete bitCoin;
                    delete user; 
                    free(line);               
                    return 0;
                }                
                user->addBitCoin(bitCoin);
            }
        }
        vct.add(user);
    }
    free(line);
    fclose(bitCoinBalancesFile);

    FILE *transactionsFile;

    if((transactionsFile = fopen(transactionsFl,"r")) == NULL) {
        perror("There was problem with the transactions file");
        vct.clear();
        return 1;
    }

    struct tm tmst;
    int amount;
    char *trId,*sendId,*recId;

    MyVector<Transaction*> trVct; //Vector where we store Transaction pointers
    Hash senderHashTable(senderHashtableNumOfEntries,bucketSize),receiveHashTable(receiverHashtableNumOfEntries,bucketSize); //HashTables

    /*
     * Here we parse transactions from the file
     */

    while(fscanf(transactionsFile,"%ms %ms %ms %d %d-%d-%d %d:%d",&trId,&sendId,&recId,&amount,&(tmst.tm_mday),&(tmst.tm_mon),&(tmst.tm_year),&(tmst.tm_hour),&(tmst.tm_min)) != EOF) {
        Transaction *tr = new Transaction(trId,sendId,recId,amount,tmst); 
              
        /*
         * Checking if transactionId exists already
         */
        if(senderHashTable.searchTransaction(tr->transactionId)) {
            std::cerr << "There is already a transaction with transactionId " << trId << std::endl;
            delete tr;
            continue;
        }

        /*
         * Checking if transaction is valid,and by valid we mean 
         * amount is positive,sender is different than receiver,money are sufficient to make the transaction
         * If it's valid,we make the transaction, otherwise we don't
         */
        if(!isValidTransaction(vct,tr)) {
            std::cerr << "Transaction " << tr->transactionId << " was unsuccessful"<<std::endl;
            delete tr; 
            continue;
        }

        /*
         * Updating our data structures with the successful transaction
         */
        trVct.add(tr);
        senderHashTable.insertItem(tr->senderId,tr);
        receiveHashTable.insertItem(tr->receiverId,tr);
    }

    fclose(transactionsFile);

    len = 0;
    char* command = NULL;

    int amnt;
    srand(time(NULL));

    //Here we wait for user's input from keyboard
    while(true) {

        std::cout<< "Give command:";
        if((read = getline(&command, &len, stdin)) != -1) {
            char* whole_command = strtok(command,"\n");
            char* token = strtok(whole_command,DELIM);

            if(token == NULL) {
                 std::cerr << "You didn't give any input" << std::endl;
                 continue;
            }

            //Checking all user's choices 
            if(strcmp(token,"/requestTransaction") == 0) {
                token = strtok(NULL,TRANSACTION_DELIM);
                requestTransaction(senderHashTable,receiveHashTable,trVct,vct,token);
            }
            else if(strcmp(token,"/requestTransactions") == 0) {
                requestTransactions(senderHashTable,receiveHashTable,trVct,vct,token);
            }
            else if(strcmp(token,"/findEarnings") == 0) {
               findTransactions(receiveHashTable,token);
            }
            else if(strcmp(token,"/findPayments") == 0) {
               findTransactions(senderHashTable,token);
            }
            else if(strcmp(token,"/walletStatus") == 0) {
                token = strtok(NULL,DELIM);
                if((amnt=walletStatus(vct,token)) >=0)
                    std::cout<<"User "<<token<<" has "<<amnt<<" money"<<std::endl;
            }
            else if(strcmp(token,"/bitCoinStatus") == 0) {
                
            }
            else if(strcmp(token,"/traceCoin") == 0) {
                
            }
            else if(strcmp(token,"/exit") == 0) {
                if(strtok(NULL,DELIM) != NULL) {
                    std::cerr << "Wrong format of command" <<std::endl;
                    continue;
                }
                //Free all the allocated memory
                free(command);
                vct.clear();
                trVct.clear();
                return 0;
            }
            else {
                std::cerr << "There is no such command" << std::endl;
            }
        }
    }
}
