#include <iostream>
#include <cstdlib>
#include <cstring>
#include "../inc/transaction.hpp"


Transaction::Transaction(char* trId,char* sendId,char* recId,int amnt,struct tm tmst) {
    // transactionId = (char*) malloc((strlen(trId)+1) * sizeof(char));
    // strcpy(transactionId,trId);

    // senderId = (char*) malloc((strlen(sendId)+1) * sizeof(char));
    // strcpy(senderId,sendId);

    // receiverId = (char*) malloc((strlen(recId)+1) * sizeof(char));
    // strcpy(receiverId,recId);


    transactionId = trId;
    senderId = sendId;
    receiverId = recId;
    amount = amnt;
    tmstamp = tmst;
}

Transaction::~Transaction() {
    free(transactionId);
    free(receiverId);
    free(senderId);
}
