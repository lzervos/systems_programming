#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <signal.h>
#include "../inc/functions.hpp"
#include "../inc/myvector.hpp"

//Checking if a file exists
bool fileExists(const char* fileName) {
    struct stat buffer;
    int exist = stat(fileName,&buffer);

    if(exist == 0)
        return true;
    else
        return false;
}

//Client's signal handling to delete his common directory file and mirror directory
void catchInterrupt(int signo) {
    if (signo == SIGQUIT || signo == SIGINT) {
        unlink(clientFullFileName);
        rmDirRecursively(mirror_dir);

        int clId;
        FILE *lg = fopen(log_file,"rw+");
        if (fscanf(lg,"%d",&clId) != 1) 
            std::cout<<"Couldn't update logfile"<<std::endl;
        else 
            fseek(lg,1,SEEK_CUR);
            fprintf(lg,"%d\n",0);
        fclose(lg);
        free(clientFileName);
        free(clientFullFileName);
        exit(0);
    }
}

//That is the code to delete a directory like 'rm -rf <directory_name>' would have done
int rmDirRecursively(const char *dirname) {
    DIR *dir;
    struct dirent *entry;
    char *path;

    if ((dir = opendir(dirname)) == NULL) {
        if(errno != EEXIST)
            std::cout<< "Couldn't delete mirror directory" << std::endl;
        return -1;
    }
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            int size;
            if ((size=asprintf(&path,"%s/%s", dirname, entry->d_name)) == -1) {
                std::cout << "Couldn't create the filename" << std::endl;
                return -1;
            }
            if (entry->d_type == DT_DIR) {
                rmDirRecursively(path);
            }
            unlink(path);
            free(path);
        }
    }
    closedir(dir);
    rmdir(dirname);
    return 0;
}